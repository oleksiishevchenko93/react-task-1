import React from 'react';

import styles from './Button.module.css';

const Button = ({ buttonText, callback = () => {}, className }) => {
	return (
		<button onClick={callback} className={`${styles.btn} ${styles[className]}`}>
			{buttonText}
		</button>
	);
};

export default Button;
