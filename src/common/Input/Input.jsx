import React from 'react';

import styles from './Input.module.css';

const Input = ({ labelText, placeholderText, onChange, value, type, name }) => {
	return (
		<div className={styles.inputField}>
			<label htmlFor={labelText}>{labelText}</label>
			<input
				name={name}
				className={styles.input}
				id={labelText}
				type={type || 'text'}
				min={0}
				placeholder={placeholderText}
				onChange={onChange}
				value={value}
			/>
		</div>
	);
};

export default Input;
