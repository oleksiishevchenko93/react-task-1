import React, { useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import UserItem from './components/UserItem/UserItem';
import { getDuration } from '../../helpers/pipeDuration';
import { Snackbars } from '../../common/Alert/Alert';
import { BUTTONS_TEXT, LABEL_TEXT } from '../../constants';

import styles from './CreateCourse.module.css';

const CreateCourse = ({
	course,
	allAuthors,
	handleChangeCourse,
	addAuthor,
	addCourse,
	deleteAuthor,
}) => {
	const { title, description, duration, authors } = course;
	const [authorsList, setAuthorsList] = useState(allAuthors);
	const [newAuthor, setNewAuthor] = useState({
		id: '',
		name: '',
	});
	const [alert, setAlert] = useState(false);
	const [alertMessage, setAlertMessage] = useState('Welcome!');
	const [severity, setSeverity] = useState('info');
	const { CREATE_COURSE, CREATE_AUTHOR, ADD_AUTHOR, DELETE_AUTHOR } =
		BUTTONS_TEXT;
	const { TITLE, AUTHOR_NAME, DESCRIPTION, DURATION } = LABEL_TEXT;

	const createAuthor = async () => {
		hideAlert();
		if (!newAuthor.name.trim() || newAuthor.name.trim().length <= 2) {
			setAlertMessage('Invalid Data!');
			setSeverity('warning');
		} else {
			setAuthorsList([...authorsList, newAuthor]);
			setNewAuthor({
				id: '',
				name: '',
			});
			setAlertMessage(`You have added ${newAuthor.name} successfully!`);
			setSeverity('success');
		}
	};

	useEffect(() => {
		showAlert();
	}, [alert]);

	const hideAlert = () => {
		setAlert(false);
	};

	const showAlert = () => {
		setAlert(true);
	};

	return (
		<div className={styles.container}>
			{alert && (
				<Snackbars show={alert} message={alertMessage} severity={severity} />
			)}
			<div className={styles.description__content}>
				<Input
					name='title'
					value={title}
					onChange={handleChangeCourse}
					labelText={TITLE}
					placeholderText={TITLE}
				/>
				<Button buttonText={CREATE_COURSE} callback={addCourse} />
			</div>
			<div className={styles.description__content}>
				<label className={styles.label} htmlFor='textarea'>
					{DESCRIPTION}
				</label>
				<textarea
					className={styles.textarea}
					name='description'
					id='textarea'
					rows='5'
					value={description}
					placeholder={DESCRIPTION}
					onChange={handleChangeCourse}
				/>
			</div>
			<div className={styles.authors__content}>
				<div className={styles.left__box}>
					<div className={styles.add__author}>
						<h2>Add Author</h2>
						<Input
							value={newAuthor.name}
							placeholderText={AUTHOR_NAME}
							labelText={AUTHOR_NAME}
							onChange={(event) =>
								setNewAuthor({ id: uuidv4(), name: event.target.value })
							}
						/>
						<Button
							callback={createAuthor}
							buttonText={CREATE_AUTHOR}
							className={'blue'}
						/>
					</div>
					<div className={styles.duration}>
						<h2>{DURATION}</h2>
						<Input
							name='duration'
							value={duration}
							placeholderText={DURATION}
							labelText={DURATION}
							onChange={handleChangeCourse}
							type='number'
						/>
						<p className={styles.duration__value}>
							{DURATION}: <strong>{getDuration(duration)}</strong> hours
						</p>
					</div>
				</div>
				<div className={styles.right__box}>
					<div className={styles.authors}>
						<h2>Authors</h2>
						{authorsList.map((author, index) => {
							return (
								<UserItem
									key={author.id}
									buttonText={ADD_AUTHOR}
									cb={addAuthor}
									author={author}
									className={'green'}
								/>
							);
						})}
					</div>
					<div className={styles.authors__list}>
						<h2>Course authors</h2>
						{!authors.length && <p>Authors list is empty</p>}
						{!!authors.length &&
							authors.map((authorId) => {
								const author = authorsList.find((element) => {
									return element.id === authorId;
								});
								return (
									<UserItem
										key={author.id}
										buttonText={DELETE_AUTHOR}
										cb={deleteAuthor}
										author={author}
										className={'red'}
									/>
								);
							})}
					</div>
				</div>
			</div>
		</div>
	);
};

export default CreateCourse;
