import React from 'react';

import Button from '../../../../common/Button/Button';

import styles from './UserItem.module.css';

const UserItem = ({ author, cb, buttonText, className }) => {
	return (
		<div className={styles.container}>
			<p className={styles.user}> {author.name}</p>
			<Button
				buttonText={buttonText}
				callback={() => cb(author)}
				className={className}
			/>
		</div>
	);
};

export default UserItem;
