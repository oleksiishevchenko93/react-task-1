import React from 'react';

import logoUrl from '../../../../assets/education-icon.png';

import styles from './Logo.module.css';

const Logo = () => {
	return <img src={logoUrl} alt='courses_logo' className={styles.logo} />;
};

export default Logo;
