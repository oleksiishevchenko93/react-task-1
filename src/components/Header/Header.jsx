import React from 'react';

import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import { userName } from '../../constants';

import styles from './Header.module.css';

const Header = () => {
	return (
		<div className={styles.header}>
			<Logo />
			<div className={styles.userName}>
				<h2>{userName}</h2>
				<Button buttonText='log out' className={'green'} />
			</div>
		</div>
	);
};

export default Header;
