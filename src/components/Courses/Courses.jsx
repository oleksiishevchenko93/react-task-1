import React, { useEffect, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import CreateCourse from '../CreateCourse/CreateCourse';
import Button from '../../common/Button/Button';
import { Snackbars } from '../../common/Alert/Alert';
import { generateDate } from '../../helpers/dateGenerator';
import {
	mockedAuthorsList,
	mockedCoursesList,
	BUTTONS_TEXT,
} from '../../constants';

import styles from './Courses.module.css';

const Courses = () => {
	const [coursesList, setCoursesList] = useState(mockedCoursesList);
	const [searchValue, setSearchValue] = useState('');
	const [searchCoursesList, setSearchCoursesList] = useState([]);
	const [search, setSearch] = useState(false);
	const [createNewCourse, setCreateNewCourse] = useState(false);
	const [authorsList, setAuthorsList] = useState(mockedAuthorsList);
	const [alert, setAlert] = useState(false);
	const [alertMessage, setAlertMessage] = useState('Welcome!');
	const [severity, setSeverity] = useState('info');
	const { ADD_COURSE } = BUTTONS_TEXT;
	const [course, setCourse] = useState({
		id: '',
		title: 'title',
		description: 'description',
		creationDate: '',
		duration: 65,
		authors: [],
	});

	const searchCourses = () => {
		if (!searchValue.trim()) {
			return setSearch(false);
		}
		const result = coursesList.filter((course) => {
			return (
				course.title.toLowerCase().includes(searchValue.toLowerCase()) ||
				course.id.toLowerCase().includes(searchValue.toLowerCase())
			);
		});
		setSearchCoursesList(result);
		setSearch(true);
	};

	const handleChange = (event) => {
		setSearchValue(event.target.value);
		if (!event.target.value.trim()) {
			setAlertMessage('Empty Value!');
			setSeverity('warning');
			setSearch(false);
		}
	};

	const handleChangeCourse = (event) => {
		setCourse({
			...course,
			[event.target.name]: event.target.value,
		});
	};

	const addAuthor = (author) => {
		setAlert(false);
		if (course.authors.includes(author.id)) {
			setAlertMessage('This author has been already added!');
			setSeverity('warning');
			return;
		}
		setCourse({
			...course,
			authors: [...course.authors, author.id],
		});
		const checking = authorsList.find((element) => {
			return element.id === author.id;
		});
		if (!checking) {
			setAuthorsList([...authorsList, author]);
		}
		setAlertMessage(`${author.name} has been added successfully!`);
		setSeverity('success');
	};

	const deleteAuthor = (author) => {
		setAlert(false);
		const result = course.authors.filter((authorId) => {
			return authorId !== author.id;
		});

		setCourse({
			...course,
			authors: result,
		});

		setAlertMessage(`${author.name} has been removed successfully!`);
		setSeverity('success');
	};

	const showInfoCourse = (authors) => {
		setAlert(false);
		setAlertMessage(`${authors}`);
		setSeverity('info');
	};

	useEffect(() => {
		setAlert(true);
	}, [alert]);

	const validateCourse = (course) => {
		if (!course.title.trim() || course.title.trim().length <= 2) {
			setAlertMessage('"Title" field is invalid!');
			return;
		}
		if (!course.description.trim() || course.description.trim().length <= 2) {
			setAlertMessage('"Description" field is invalid!');
			return;
		}
		if (course.duration <= 0) {
			setAlertMessage('"Duration" field should be more than 0!');
			return;
		}
		if (!course.authors.length) {
			setAlertMessage('"Authors" field is invalid!');
			return;
		}
		return true;
	};

	const addCourse = () => {
		setAlert(false);
		const isValid = validateCourse(course);
		if (!isValid) {
			setSeverity('error');
			return;
		}
		setCoursesList([
			...coursesList,
			{
				...course,
				id: uuidv4(),
				creationDate: generateDate(),
			},
		]);
		setAlertMessage('New course has been added successfully!');
		setSeverity('success');
		toggleCreatePage();
		setCourse({
			id: '',
			title: '',
			description: '',
			creationDate: '',
			duration: 0,
			authors: [],
		});
	};

	const toggleCreatePage = () => {
		setCreateNewCourse(!createNewCourse);
	};

	if (createNewCourse) {
		return (
			<div className={styles.courses}>
				{alert && (
					<Snackbars show={alert} message={alertMessage} severity={severity} />
				)}
				<CreateCourse
					course={course}
					allAuthors={authorsList}
					handleChangeCourse={handleChangeCourse}
					deleteAuthor={deleteAuthor}
					addAuthor={addAuthor}
					addCourse={addCourse}
				/>
			</div>
		);
	}

	return (
		<div className={styles.courses}>
			{alert && (
				<Snackbars show={alert} message={alertMessage} severity={severity} />
			)}
			<div className={styles.topBar}>
				<SearchBar
					onClick={searchCourses}
					onChange={handleChange}
					value={searchValue}
				/>
				<Button
					callback={toggleCreatePage}
					buttonText={ADD_COURSE}
					className={'green'}
				/>
			</div>
			{!search &&
				coursesList.map((course) => {
					return (
						<CourseCard
							key={course.id}
							course={course}
							authorsList={authorsList}
							showInfoCourse={showInfoCourse}
						/>
					);
				})}
			{search &&
				searchCoursesList.map((course) => {
					return (
						<CourseCard
							key={course.id}
							course={course}
							authorsList={authorsList}
						/>
					);
				})}
		</div>
	);
};

export default Courses;
