import React from 'react';

import Button from '../../../../common/Button/Button';
import { getDuration } from '../../../../helpers/pipeDuration';
import { changeDate } from '../../../../helpers/dateGenerator';
import { BUTTONS_TEXT } from '../../../../constants';

import styles from './CourseCard.module.css';

const CourseCard = ({ course, authorsList, showInfoCourse }) => {
	const { title, description, creationDate, duration, authors } = course;
	const { SHOW_COURSE } = BUTTONS_TEXT;
	const getAuthors = (authors) => {
		const authorsData = authorsList.filter((author) => {
			return authors.includes(author.id);
		});
		const authorsArray = authorsData.map((author) => {
			return author.name;
		});
		let result = authorsArray.join(', ').split('');

		if (result.length > 24) {
			result.splice(24, Infinity, '...');
		}

		return result.join('');
	};

	return (
		<div className={styles.courseCard}>
			<div className={styles.courseCard__content}>
				<h2>{title}</h2>
				<p>{description}</p>
			</div>
			<div className={styles.courseCard__info}>
				<p>
					<strong>Authors: </strong>
					{getAuthors(authors)}
				</p>
				<p>
					<strong>Duration: </strong>
					{getDuration(duration)} hours
				</p>
				<p>
					<strong>Created: </strong>
					{/*{creationDate}*/}
					{changeDate(creationDate)}
				</p>
				<Button
					buttonText={SHOW_COURSE}
					className={'blue'}
					callback={() => showInfoCourse(getAuthors(authors))}
				/>
			</div>
		</div>
	);
};

export default CourseCard;
