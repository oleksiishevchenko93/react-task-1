export const getDuration = (duration) => {
	if (duration < 0) {
		return '00:00';
	}
	const MINUTES = 60;
	let getMinutes = Number(duration) % MINUTES;
	let hours = (Number(duration) - getMinutes) / MINUTES;

	if (getMinutes < 10) {
		getMinutes = `0${getMinutes}`;
	}
	if (hours < 10) {
		hours = `0${hours}`;
	}

	return `${hours}:${getMinutes}`;
};
