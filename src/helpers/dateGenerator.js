export const generateDate = () => {
	return new Date(Date.now()).toLocaleDateString('en-GB');
};
// new Date().toLocaleDateString('en-GB')
export const changeDate = (date = generateDate()) => {
	return date.replaceAll('/', '.');
};
